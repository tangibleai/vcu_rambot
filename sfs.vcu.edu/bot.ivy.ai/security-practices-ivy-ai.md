# Security Practices

We take security seriously here at ivy.ai, and for good reason: every person using our service expects their data to be secure and confidential. We understand how important the responsibility of safeguarding this data is to our clients and work to maintain that trust. For more information on our commitment to providing secure services, please review the following Security Practices, along with our [Privacy Policy](https://ivy.ai/privacy) and [Terms of Service](https://ivy.ai/terms-and-conditions).  
  
If you believe you have found a security vulnerability in Ivy, please contact our security team.  
  
Confidentiality  
We place strict controls over our employees’ access to the data you and your users make available via Ivy, as more specifically defined in your agreement with Ivy covering the use of the Ivy services  
(“Customer Data”), and are committed to ensuring that Customer Data is not seen by anyone who should not have access to it. The operation of the Ivy requires that some employees have access to the systems which store and process Customer Data. For example, in order to diagnose a problem you are having with the Ivy chatbot, we may need to access your Customer Data. These employees are prohibited from using these permissions to view Customer Data unless it is necessary to do so. We have technical controls and audit policies in place to ensure that any access to Customer Data is logged.  
  
All of our employees and contract personnel are bound to our policies regarding Customer Data and we treat these issues as matters of the highest importance within our company.  
  
Personnel Practices  
Ivy conducts background checks on all employees before employment, and employees receive security training during onboarding as well as on an ongoing basis. All employees are required to read and sign our comprehensive information security policy covering the security, availability, and confidentiality of the Ivy services.  
  
Compliance  
The environment that hosts the Ivy services maintains multiple certifications for its data centers,  
including ISO 27001 compliance, PCI Certification, and SOC reports. For more information about their certification and compliance, please visit the [Google Cloud Security website](https://cloud.google.com/security) and the  
[Google Cloud Compliance website](https://cloud.google.com/security/compliance).  
  
Security Features for Account Owners and Administrators  
In addition to the work we do at the infrastructure level, we provide Administrators of the Ivy services with additional tools to enable their own users to protect their Customer Data.  
  
**Access Logging**  
Detailed access logs are available to administrators. We log every time a user interacts with Ivy,  
noting the type of device used and the IP address of the connection. Administrators can review  
consolidated access logs for their bot implementation.  
  
**Administrator Two-Factor Authentication**  
Account owners can require all administrators to set up two-factor authentication on their accounts.  
  
**Single Sign On**  
Administrators of paid teams can integrate their Ivy instance with a variety of single-sign-on providers with any SAML 2.0 compliant provider.  
  
**Deletion of Customer Data**  
Ivy provides the option for Account Owners to delete Customer Data at any time during a subscription term. Within 24 hours of Account Owner initiated deletion, Ivy hard deletes all information from currently-running production systems. Ivy services backups are destroyed after 14 days.  
  
**Return of Customer Data**  
During a subscription term administrators of any Ivy services plan can export Customer Data.  
  
Data Encryption in Transit and at rest  
The Ivy services support the latest recommended secure cipher suites and protocols to encrypt all traffic in transit. Customer Data is encrypted at rest.  
  
We monitor the changing cryptographic landscape closely and work promptly to upgrade the service to respond to new cryptographic weaknesses as they are discovered and implement best practices as they evolve. For encryption in transit, we do this while also balancing the need for compatibility for older clients.  
  
Availability  
We understand that your Students rely on the Ivy services to ask questions. We’re committed to making Ivy a highly-available service that you can count on. Our infrastructure runs on systems that are fault tolerant, for failures of individual servers or even entire data centers. Our operations team tests disaster-recovery measures regularly and staffs an on-call team to quickly resolve unexpected  
incidents.  
  
Disaster Recovery  
Customer Data is stored redundantly at multiple locations in our hosting provider’s data centers to  
ensure availability. We have well-tested backup and restoration procedures, which allow recovery from a major disaster. Customer Data and our source code are automatically backed up nightly. The Operations team is alerted in case of a failure with this system. Backups are fully tested at least every 90 days to confirm that our processes and tools work as expected.  
  
Network Protection  
In addition to sophisticated system monitoring and logging, we have implemented two-factor authentication for all server access across our production environment. Firewalls are configured according to industry best practices and unnecessary ports are blocked by configuration with Google Cloud Security Groups.  
  
Host Management  
We perform automated vulnerability scans on our production hosts and remediate any findings that present a risk to our environment. We enforce screens lockouts and the usage of full disk encryption for company laptops.  
  
Logging  
Ivy maintains an extensive, centralized logging environment in its production environment which contains information pertaining to security, monitoring, availability, access, and other metrics about the Ivy services. These logs are analyzed for security events via automated monitoring software, overseen by the security team.  
  
Incident Management and Response  
In the event of a security breach, Ivy will promptly notify you of any unauthorized access to your  
Customer Data. Ivy has incident management policies and procedures in place to handle such an event.  
  
External Security Audits  
We contract with respected external security firms who perform regular audits of the Ivy services to  
verify that our security practices are sound and to monitor the Ivy services for new vulnerabilities  
discovered by the security research community. In addition to periodic and targeted audits of the Ivy  
services and features, we also employ the use of continuous hybrid automated scanning of our web  
platform.  
  
Product Security Practices  
New features, functionality, and design changes go through a security review process facilitated by the security team. In addition, our code is audited with automated static analysis software, tested, and manually peer-reviewed prior to being deployed to production. The security team works closely with development teams to resolve any additional security concerns that may arise during development