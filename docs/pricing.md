RamBot probably cost VCU $10k-$30k + $25/mo/user (school administrators).

Here's what we would likely charge...

## Data

One-time data import into the chatbot (typical for a Customer Management System):

-   1,000 records: $500
-   10,000 records: $2,500
-   100,000 records: $10,000
-   1,000,000+ records: $25,000

## Training

Group training session lasts 2 hours with a 5 minute break/exercise every 15 minutes.
On-on-one sessions last 1 hour with one 5 minute break. First half is demonstration and second half is admin user sharing screen to work through the demonstrated tasks.

- Free: Offline video self-training
- $100: 1 hr session 1:1 training remote
- $500: 2 hr group training remote
- $1500 (+travel): 2 hr group training *in person*

--------------------------------------

Competitors:
Zendesk $20-30/mo/agent
Tidio $16-$42/mo/agent
Qualified for Salesforce $3k/year?
Landbot
ChatFunnels $500/yr?
Outgrow $14/mo
Botmaker $149/mo
Qlik Sense $30/mo
Typeform chatbot $400/yr

### Customer Relationship Management (CRM) Software Price Range

Most CRM software in the market are priced on a per-user, per-month basis; hence, businesses can expect to pay between the range of $10 to $200 per user monthly. There are also some CRM products with yearly pricings and others with perpetual licenses.

The above price range depends on the sophistication of the CRM products feature-wise, as well as the number of user accesses or accounts paid for. Some CRM software within the above price ranges are Zendesk Sell, which starts from $19 monthly per user, FreeAgent CRM pricing starts from $25 monthly per user, and GreenRope costs between $149 for up to 1,000 contacts to $699 for up to 50,000 contacts.

In addition, CRM products target different sizes of organizations, and their price ranges are as follows:

-   **Small Businesses** can expect to pay between the range of $10 to $25 for CRM software. The prices are set on a per user per month basis. For example, Zoho CRM costs $12 per user per month, Fresh Sales costs $13 per user, per month, and Copper pricing starts $19 per user per month. In addition to this, some products offer small businesses very basic packages which can be gotten for free.
-   **Medium Businesses** can expect to pay between $25 to $50 for CRM software. While most products for medium-sized businesses are priced on a per user, per month basis, the number of employees or registered users will determine the prize of the software per user. For example, Hubspot costs up to $30 per user monthly but $50 monthly for two users. Others are Benchmark ONE, which costs $40 monthly per user, and Skynamo, which is priced at $46 per user monthly.
-   **Large Businesses** can expect prices that fall within $50 to $200 for CRM products. CRM offerings for large business organizations typically come with more features; hence, there may be separate fees for installation, setup, or training. Some CRM software within this tier are FreeAgent CRM, which costs $130 price monthly per user; GreenRope costs between $149 for up to 1,000 contacts to $699 for up to 50,000 contacts , and Quick Base pricing falls around $500 monthly for a number of users.

#### CRM best of breeds and their price ranges are as follows:

-   **Marketing Automation Software** Marketing automation products can be divided into three tiers, each carrying prices that range from $0 to $19, $19 to $50, and $50 upwards, respectively. For instance, Active Campaign pricing starts from $15 monthly per user, Hubspot Marketing Automation costs up to $50 per user monthly, and Integrated Marketing Portal pricing is around $50 per user monthly.
-   **Lead Management Software** The pricing for Lead management products in the market usually varies between $11 and $60 upwards. These price ranges depend on the level of the products offerings and the number of registered users per month. For example, SalesExec pricing starts from $65 per user monthly, Salesforce Essentials costs around $25 per user monthly, and Lead Capsule pricing starts from around $500 per month for a number of years.
-   **SalesForce Automation Software** SalesForce automation products pricing ranges from $13 to $50 upwards, depending on the level of their offerings and the number of registered accounts per month. The prices are set per user, per month; hence, SAA software pricing like Really Simple System starts from $14 per user monthly, Pepperi pricing is around $48 per user monthly, and Cliently costs up to $39 per user per month.
-   **Contact Management Software** Most contact management products in the market are priced on a per month basis, and there are typically set limits to the number of contacts each user can manage on the software. Depending on the level of offers, prices may fall between $0 to $14, $14 to $25, and $25 to $400. For example, Infloflo pricing starts from around $100 per month, DejaOffice PC CRM costs up to $50 monthly, and AirTable pricing starts from $14 per month.
-   **Email Marketing Software** Email marketing products are priced per month, and there are usually limits on the number of subscribers or emails for each user monthly. The prices range from $0 and $30 upwards, depending on the package level. For example, Constant Contact pricing starts from $20 per month, SendinBlue pricing starts from $25 per month, and Campaigner costs around $19 per month.

### What is the cost for AI: Customer Support implementation?

When it comes to selecting AI: Customer Support system, buyers are primarily concerned about the cost. In fact, it is the cost that determines whether a potential buyer would go with the product. True, there is no one-size-fits-all formula to determine the “worth” of a business application, but as a software buyer, you want to make sure you get the best value for your money, without having to dig a big hole in your pocket.  
  
Understanding the exact price of a AI: Customer Support system isn't easy as the overall cost of software includes the cost of a license, subscription fees, training, customization, hardware, maintenance, support, and other related services. It's essential to take into account all of these costs to gain an understanding of the system's "total cost of ownership."

### What are the typical AI: Customer Support pricing models?

There are primarily three common pricing models – Perpetual License, Subscription, and Commercial Open Source.  
  

-   **Subscription/Software-As-A-Service: - Relevant for Ivy.ai**  
    Under this pricing model, the system is accessed over the internet, as opposed to installed on-premises. The payment is made either on a per user basis or subscription basis. Ideally, customers are required to pay a recurring monthly fee until a specific period for using the tool. Subscription pricing model is more common with Software-as-a-Service (SaaS) apps.
-   Upfront cost for customization and integration is less compared to perpetual license cost because there is not much flexibility with SaaS systems in this area.
-   Recurring cost is greater as customers are required to make monthly payments as a subscription fee. Additionally customers using premium support services must pay an extra fee.
-   All in all, the total cost of ownership in the both cases is almost the same and may span over a period of 7-10 years, though you may have to pay a higher perpetual license fee upfront. The total cost may vary from starter to mid range to enterprise level apps in both cases.
-   **Perpetual license: - Relevant for Ivy.ai**  
    A common pricing model for on-premise applications, perpetual license requires a customer to pay an upfront sum to own the tool or other intellectual property on-premises for a fixed term.
-   Upfront cost involves the fee for installation, customization, and integration with existing systems, besides perpetual license fee.
-   Recurring cost is low in this pricing model and may include the cost for updates, maintenance, upgrades, and patches. Some vendors do offer premium support services, which come for an extra price.
-   **Commercial open source: Not relevant for Ivy.ai**  
    The customer can acquire the system free of cost without having to incur any upfront license fee. As a customer, you’re solely responsible for the ongoing maintenance, upgrading, customization, and troubleshooting of the application to meet your specific needs. You are on your own for providing end-user support, since you are not locked in with a vendor-supplied system solution.

### How much would it cost to customize Ivy.ai? (and is it relevant)

If you need specific features in your system catering to your specific business requirements, the vendor will charge customization cost, depending on your needs and feature requirement. Ideally customization cost is more complex to calculate compared to licensing cost.  
  
Some apps allow you to easily combine data from multiple sources, without any complicated query requirements, while some others can be embedded into different applications to provide enhanced reporting. If you seek products that support customizable dashboards and predictive analysis to identity possible trends and facilitate decision making, you may have to pay higher for all the customization features.  
  
Additionally, the following factors may affect the cost of customization:

-   User interface changes
-   Configurable dashboards
-   Data elements required for tracking
-   Forms to collect additional data
-   Dashboard, management and operational reports that are needed.
-   Workflows and how complex they are
-   Forms to collect additional data

**Here are some questions to answer:** How much customization is needed? How many systems do you want to integrate to? Does your company work like industry standards or does it have its own customized processes? What kind of special reports are needed?  

In order to calculate the cost of customization you can use the following estimates:

-   Minimal customization - integrate with 1-2 systems: $2,500
-   Standard customization - integrate with 3-5 systems: $10,000
-   Fully customized system - integrate with more than 5 systems: $25,000

### What is the data migration cost of Ivy.ai? Relevant for Ivy.ai

Most companies opt for data migration services from a vendor, which raises the cost of product ownership. If you choose to transfer data on your own, you can avoid paying the cost of data migration.  
Data migration cost depends on the amount of data to be transferred, your current software, availability of migration tools, complexity of data, and gaps between the existing system and the new system.  
  
If your data is stored in Excel spreadsheets, then it may incur you a lot of time and money to migrate data from Excel.  
By involving a business services provider in data migration, you are asking them to offer additional services, for which you may have to pay extra.  

As a rule of thumb the cost of data migration depends on how many records you want to migrate. Records can include the number of customers, invoices, financial transactions, products, versions, etc. Here is a list you can use as a rule of thumb:

-   1,000 records: $500
-   10,000 records: $2,500
-   100,000 records: $10,000
-   1,000,000+ records: $25,000

### What is Ivy.ai's cost of training? Relevant for Ivy.ai

As a buyer, you are required to pay extra for in-person training, though some vendors offer web-based solutions as part of the package. The cost may involve end-user training, video/self, group, department, and training the super users.  
  
The cost is mainly derived from the approach that you select for your organization:

-   End-user training
-   Group/Department
-   Video /self
-   Train the trainer/super user

  
**Here are some questions to answer:** How many groups (different departments, usages, type of users) are needed?  

In order to calculate the cost you can use the following estimates:

-   1-2 Sessions: $500
-   3-4 Sessions: $1,500
-   5-7 Sessions: $2,500
-   8-10 Sessions: $5,000

### How Ivy.ai pricing compares to alternative AI: Customer Support solution?

When comparing Ivy.ai to alternative systems, **on a scale between 1 to 10 Ivy.ai is rated 4**

### How can the team at ITQlick help?

**The science of TCO (total cost of ownership) may not be easy to calculate. If you seek to get detailed info about the TCO, get in touch with ITQlick experts. Contact us today and get up to date, detailed quotes.**